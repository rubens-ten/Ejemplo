package com.ejemplo.beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import com.ejemplo.clases.Usuario;

@ManagedBean (name="beanUsuario")
@ViewScoped	
public class UsuarioBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8602752169649891049L;

	//Propiedades de la clase
	private Integer idUsuario;
	
	//Usuario encontrado
	private Usuario usuarioEncontrado;
	
	//Lista de usuarios
	private List<Usuario> listUsuario;
	
	//Constructor de la clase
	public UsuarioBean(){}
	
	//PostConstruct
	@PostConstruct
	public void init(){
		
		if(listUsuario == null){
			listUsuario = new ArrayList<Usuario>();
		}
		
		//Coleccion de usuarios
		for(int i = 0; i < 20; i++){
			Usuario usuario = new Usuario();
			usuario.setIdUsuario(i);
			usuario.setNombre("Usuario " + i);
			usuario.setApellido("Perenganito " + i );
			usuario.setEdad(10+i);
			listUsuario.add(usuario);
		}
		
	}
	
	public void busqueda(){
		setUsuarioEncontrado(null);
		//Se itera sobre la lista para encontrar al usuario
		for(Usuario user : listUsuario){
			//Si el id del usuario existe en la lista
			if(user.getIdUsuario() == getIdUsuario()){
				System.out.println("Usuario encontrado");
				//El objeto usuario seleccionado toma el valor del usuario encontrado
				setUsuarioEncontrado(user);
				System.out.println("Usuario no encontrado");
				break;
			}
		}
		
		if(getUsuarioEncontrado() == null){
			System.out.println("Usuario no encontrado");
		}
	}

	//Setters y getters
	public Integer getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(Integer idUsuario) {
		this.idUsuario = idUsuario;
	}

	public List<Usuario> getListUsuario() {
		return listUsuario;
	}

	public void setListUsuario(List<Usuario> listUsuario) {
		this.listUsuario = listUsuario;
	}

	public Usuario getUsuarioEncontrado() {
		return usuarioEncontrado;
	}

	public void setUsuarioEncontrado(Usuario usuarioEncontrado) {
		this.usuarioEncontrado = usuarioEncontrado;
	}
}//fin de la clase
